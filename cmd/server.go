package cmd

import (
	"github.com/spf13/cobra"
	"padro/bootstrap"
)

var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "run server",
	Long:  `thank you use my application`,
	Run: func(cmd *cobra.Command, args []string) {
		bootstrap.Setup()
	},
}

func init() {
	rootCmd.AddCommand(serverCmd)
}
