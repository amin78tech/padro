package cmd

import (
	"github.com/spf13/cobra"
	"padro/provider/Database"
	"padro/provider/config"
)

var migrationCmd = &cobra.Command{
	Use:   "migrate",
	Short: "start migration database",
	Long:  `please use migration command for create database struct`,
	Run: func(cmd *cobra.Command, args []string) {
		config.Init()
		Database.Init()
		Database.RunMigration()
	},
}

func init() {
	rootCmd.AddCommand(migrationCmd)
}
