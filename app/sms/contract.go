package sms

type SmsContract interface {
	Send(mobile string, content string) error
}
