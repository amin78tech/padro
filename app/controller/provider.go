package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"padro/app/model"
	"padro/app/repository"
)

func ProviderController(context *gin.Context) {
	provider := model.Provider{Name: "tipax", Api: "https://run.mocky.io/v3/18357160-99bc-4bb2-8d39-41b2fe5594ef"}
	err, provider := repository.RunConstructorProvider().Create(&provider)
	if err != nil {
		context.JSON(http.StatusInternalServerError, gin.H{
			"success": false,
			"data":    nil,
		})
	} else {
		context.JSON(http.StatusOK, gin.H{
			"message": "success",
			"data":    provider,
		})
	}

}
