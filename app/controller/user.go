package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"padro/app/model"
	"padro/app/repository"
)

func UserController(context *gin.Context) {
	user := model.User{UserName: "amin78tech", Phone: "09336278343"}
	err, user := repository.RunConstructorUser().Create(&user)
	if err != nil {
		context.JSON(http.StatusInternalServerError, gin.H{
			"success": false,
			"data":    nil,
		})
	} else {
		context.JSON(http.StatusOK, gin.H{
			"message": "success",
			"data":    user,
		})
	}

}
