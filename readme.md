# pure golang (design struct and develop)
1. use job for update order status
2. use strategy pattern for sms
3. use command line interface
4. use viper for environment config

## how use? 
run command <a href="">go run app.go migrate (db name is **padro**)</a>

run command <a href="">go run app.go server </a>
<hr>

1.send request to api create user(not body)
http://127.0.0.1:8000/user

2.send request to api create provider(not body)
http://127.0.0.1:8000/provider

3.send request to api create order(not body)
http://127.0.0.1:8000/order

<hr>
run command <a href="">go run app.go job </a>

