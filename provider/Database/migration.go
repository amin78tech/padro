package Database

import (
	"fmt"
	"padro/app/model"
)

func RunMigration() {
	DatabaseInstance.AutoMigrate(&model.User{}, &model.Provider{}, &model.Order{}, &model.OrderStatus{})
	fmt.Println("Migration complete")
}
