package response

import "padro/app/enum"

type OrderResponse struct {
	Address string
	Phone   string
	Status  enum.Status
}
