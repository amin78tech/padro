package Router

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"padro/Route"
	"padro/provider/config"
)

func Init() {
	router := gin.Default()
	registerRoute(router)
	router.Run(fmt.Sprintf("%s:%s",
		config.InstanceConfig.Router.Host,
		config.InstanceConfig.Router.Port,
	))
}
func registerRoute(router *gin.Engine) {
	Route.UserRouter(router)
	Route.ProviderRouter(router)
	Route.OrderRouter(router)
}
