package cmd

import (
	"fmt"
	"github.com/go-co-op/gocron/v2"
	"github.com/spf13/cobra"
	"padro/app/job"
	"padro/provider/Database"
	"padro/provider/config"
	"time"
)

var jobCmd = &cobra.Command{
	Use:   "job",
	Short: "run job",
	Long:  `run job update status order`,
	Run: func(cmd *cobra.Command, args []string) {

		scd, _ := gocron.NewScheduler()
		job, _ := scd.NewJob(gocron.DurationJob(5*time.Second), gocron.NewTask(func() {
			job.UpdateStatusOrder()
		}))
		fmt.Println("job create", job.ID())
		scd.Start()
		select {
		case <-time.After(time.Minute):
		}
		_ = scd.Shutdown()
	},
}

func init() {
	_ = config.Init()
	Database.Init()
	rootCmd.AddCommand(jobCmd)
}
