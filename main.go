/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>

*/
package main

import "padro/cmd"

func main() {
	cmd.Execute()
}
