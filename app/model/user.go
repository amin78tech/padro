package model

import "gorm.io/gorm"

type User struct {
	gorm.Model
	Orders   []Order
	Phone    string
	UserName string `gorm:"size:255"`
}
