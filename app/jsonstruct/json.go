package jsonstruct

type Data struct {
	Id     int
	Status string
}
type ResponseApi struct {
	Code    int
	Message string
	Data    Data
}
