package repository

import (
	"fmt"
	"gorm.io/gorm"
	"padro/app/model"
	"padro/provider/Database"
)

type OrderRepository struct {
	Database *gorm.DB
}

func RunConstructorOrder() *OrderRepository {
	return &OrderRepository{Database: Database.DatabaseInstance}
}
func (OrderRepository *OrderRepository) Create(data *model.Order) (error, model.Order) {
	res := OrderRepository.Database.Create(data)
	if res.Error != nil {
		return fmt.Errorf("not created user successfully"), model.Order{}
	}
	return nil, *data
}
func (OrderRepository *OrderRepository) GetAllOrder() (error, []model.Order) {
	Orders := []model.Order{}
	res := OrderRepository.Database.Model(&model.Order{}).Preload("Provider").Find(&Orders)
	if res.Error != nil {
		return fmt.Errorf("not found all order"), []model.Order{}
	}
	return nil, Orders
}
func (OrderRepository *OrderRepository) UpdateOrderStatus(orderId int, status string) error {
	result := OrderRepository.Database.Model(&model.Order{}).Where("id = ?", orderId).Update("status", status)
	if result.Error != nil {
		return fmt.Errorf("Error in update status order")
	} else {
		return nil
	}
}
