package model

import (
	"gorm.io/gorm"
)

type OrderStatus struct {
	gorm.Model
	Id      uint `gorm:"primary_key"`
	OrderId uint
	Status  string
}
